public class Main {
    public static void main(String[] args) {
        rectangle rectA= new rectangle(5,6);
        //rectA.sideA=5;
        //rectA.sideB=6;
        System.out.println("Side A: "+rectA.sideA+ " Side B: "+rectA.sideB);

        int area=rectA.area();
        System.out.println("Area : "+area);
        System.out.println("Perimeter: "+rectA.perimeter());

        Circle circle=null;
        
        circle=new Circle(10);
        System.out.println("Area: "+circle.area());
        System.out.println("Perimeter: "+circle.perimeter());


        rectangle[] rectangles=new rectangle[6];
        rectangles[0]=rectA;
        rectangles[1]=new rectangle(5, 6);
        rectangles[2]=rectA;

        rectangle r=rectangles[0];
        System.out.println("Area : "+r.area());

        if (rectangles[0]==rectangles[1]) {
            //false;
        }
        if (r==rectangles[0]) {
            //true;
        }
        if (rectangles[0]==rectangles[2]) {
            //true;
        }
        if (rectangles[3]==rectangles[4]) {
            System.out.println("Nulls");
        }

    }
}