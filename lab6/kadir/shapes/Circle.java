package kadir.shapes;
import java.util.Random;
import java.lang.Math;
public class Circle {
    int radius;
    public Circle(int radius) {
        this.radius=radius; 
    }
    
    public double area() {
        Random random=new Random();
        return Math.PI*radius*radius;
    }

}