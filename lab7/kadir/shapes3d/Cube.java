package kadir.shapes3d;
import kadir.shapes.Square;

public class Cube extends Square{
    int side;
    public Cube()
    {
        this(5);
    }
    
    public Cube(int s) {
        super(s);
        side=s;
    }

    public int area() {
        return 6*super.area();
    }

    public int volume() {
        return side*super.area();
    }
    @Override
    public String toString() {
        return "Side="+side;
    }
    
}