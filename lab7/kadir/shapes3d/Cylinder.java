package kadir.shapes3d;
import kadir.shapes.Circle;
public class Cylinder extends Circle{

    private int height;

    public Cylinder(){
        this(5, 10);
        System.out.println("Creating a Cylinder");
    }

    public Cylinder(int r) {
        this(r, 10);
    }

    public Cylinder(int r,int h) {
        super(r);
        height=h;
    }

    public double area() {
        return 2*super.area()+2*Math.PI*radius*height;
    }

    public double volume() {
        return super.area() * height;
    }
    @Override  //daha önceden yazılmış methodlara bakıp, üzerine yazılacak method adını kontrol eder
    public String toString() {
        return "Radius="+radius+","+
                "Height="+height;
    }
    
}