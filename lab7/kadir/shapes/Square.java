package kadir.shapes;

public class Square extends Object{
    int side;

    public Square(int side) {
        super();
        this.side=side;
    }
    public int area() {
        return side*side;
    }
    
}