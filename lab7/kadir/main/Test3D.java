package kadir.main;

import java.util.ArrayList;

import kadir.shapes3d.Cube;
import kadir.shapes3d.Cylinder;

public class Test3D {
    public static void main(String[] args) {
        ArrayList <Cylinder> cylinders=new  ArrayList<>();
        Cylinder cylinder=new Cylinder();

        cylinders.add(cylinder);
        cylinders.add(new Cylinder(6,7));

        System.out.println(cylinders);

        ArrayList <Cube> cubes=new  ArrayList<>();
        Cube cube=new Cube();

        cubes.add(cube);
        cubes.add(new Cube(4));

        System.out.println(cubes);
    }
    
}