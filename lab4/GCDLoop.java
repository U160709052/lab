public class GCDLoop {

    public static void main(String[] args) {
        int a=Integer.parseInt(args[0]);
        int b=Integer.parseInt(args[1]);
        int r;
        if (a<b) {
            r=a;
            while (a!=0) {
                r=a;
                a=b%a;
                b=r;
                
            }
            System.out.println(b);
        } else {
            r=b;
            while (b!=0) {
                r=b;
                b=a%b;
                a=r;
            }
            System.out.println(a);
        }
        
    }
}