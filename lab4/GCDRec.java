public class GCDRec {
    public static void main(String[] args) {
        int x=Integer.parseInt(args[0]);
        int y=Integer.parseInt(args[1]);
        if (x>y) {
            System.out.println(GCD(x,y));
        } else {
            System.out.println(GCD(y,x));
        }
        
    }
    static int GCD(int a, int b){
        int r=b;
        if (a%b!=0) {
            b=a%b;
            b=GCD(r, b);
        }
        return b;
    }
}