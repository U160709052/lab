public class Int2Bin {
    public static void main(String[] args) {
        String method_name=args[0];
        if (method_name.equals("loop")) {
            loop(Integer.parseInt(args[1]));
        } 
        else if (method_name.equals("rec")) {
            System.out.println(rec(Integer.parseInt(args[1])));
        }
        else{
            System.out.println("Firstly, write a method name like loop or rec , and then write a number");
        }
    }
    public static void loop(int decimal) {
        String binary="";
        String remainder="";
        while (decimal!=1) {
            remainder+=decimal%2;
            decimal=decimal/2;
        }
        remainder+="1";
        for (int i = remainder.length()-1; i >=0; i--) {
            binary+=remainder.charAt(i);
        }
        System.out.println(binary);
    }
    public static String rec(int decimal) {
        String binary="";
        int remainder =1;
        if (decimal!=1) {
            remainder=decimal%2;
            decimal=decimal/2;
            binary+=rec(decimal);
        } 
        binary+=remainder;
        return binary;
    }
}