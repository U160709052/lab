public class MatrixCalculator {

    public static void main(String[] args) {
        int[][] matrixA = {{6, 8, 2}, {9, 5, 11}, {7, 2, 5}};
        int[][] matrixB = {{4, 6, 3}, {5, 8, 1}, {6, 6, 7}};
        int[][] matrixC = MatrixAddition(matrixA,matrixB);
        String line="";
        for(int i=0;i<3;i++)
        {   
            line="";
            for(int j=0;j<3;j++)
            {
                line=line+matrixC[i][j]+"  ";
            }
            System.out.println( line);
        }
    }
    public static int[][] MatrixAddition(int[][] A, int[][] B) {
        int[][] result={{0,0,0},{0,0,0},{0,0,0}};
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                result[i][j]=A[i][j]+B[i][j];
            }
        }
        return result;
    }
}